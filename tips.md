## Vim tips
* In visual mode by pressing `o` changed direction of select
* `gv` select repeat




## Other
##### FZF Installation
`git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf`<br>
`~/.fzf/install`
Install **ag** for respecting gitignore from fzf <br>
`apt-get install silversearcher-ag`
##### Vim compilation
If ycmd servershutdown
`cd ~/.vim/plugged/youcompleteme/ && ./install.py`


