set nocompatible

" ====================Plugins====================
silent! call plug#begin('~/.vim/plugged') 
" A tree explorer plugin for vim.
Plug 'scrooloose/nerdtree'
"  Defaults everyone can agree on 
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tomtom/tcomment_vim'
" Plug 'jeetsukumaran/vim-buffergator'
" Plug 'valloric/youcompleteme', { 'do': './install.py --tern-completer'  }
Plug 'ternjs/tern_for_vim'
" Plug 'jiangmiao/auto-pairs'
" Plug 'dyng/ctrlsf.vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'  }
Plug 'junegunn/fzf.vim'
" Plug 'w0rp/ale'
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
Plug 'mattn/webapi-vim'
Plug 'stephpy/vim-yaml'
Plug 'tpope/vim-obsession'
      
" Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" Appearance plugins
Plug 'vim-airline/vim-airline'
" Plug 'nanotech/jellybeans.vim'
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline-themes'
Plug 'blueyed/vim-diminactive' " highlight active split
Plug 'iamcco/markdown-preview.vim' 

" HTML & CSS
Plug 'mattn/emmet-vim'
" Plug 'gko/vim-coloresque'
Plug 'cakebaker/scss-syntax.vim'
Plug 'Valloric/MatchTagAlways'
" Plug 'Valloric/MatchTagAlways'  Require vim compiled with python

" JavaScript
Plug 'posva/vim-vue'
" Plug 'nikvdp/ejs-syntax'
Plug 'mxw/vim-jsx'
Plug 'pangloss/vim-javascript'
" Plug 'isruslan/vim-es6'

" Just For Fun
" Plug 'mhinz/vim-startify' 
"Plug 'ryanoasis/vim-devicons'
" Plug 'takac/vim-hardtime'

call plug#end()


" ====================VANILLA VIM SETTINGS====================
set guifont=DejaVuSansMono10
set encoding=utf-8
set cmdheight=1
set laststatus=2
set number
set relativenumber
set incsearch
set cursorline
set background=dark
set tabstop=2
set shiftwidth=2
set expandtab
set showcmd
set hidden
set cpoptions+=$
filetype off     
syntax enable
colorscheme gruvbox
set laststatus=2 statusline=%{ObsessionStatus()}%02n:%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
let mapleader = "\<Space>"

if !has('gui_running')
    set t_Co=256
endif

" f3: Toggle list (display unprintable characters).
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
nnoremap <F3> :set list!<CR>

" RUSSIAN KEYBOARD SUPPORTING
set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
highlight lCursor guifg=NONE guibg=Cyan

" Enable omni completion.
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
" autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
" autocmd FileType ruby setlocal omnifunc=rubycomplete#CompleteTags
" autocmd BufWritePost *.hs call s:check_and_lint()
" autocmd BufWritePost *.hs GhcModCheckAndLintAsync
" Correct autocomplete
autocmd BufNewFile,BufRead *.scss             set ft=scss.css
autocmd BufNewFile,BufRead *.sass             set ft=sass.css

"arrows off
nnoremap <up>    <nop>
nnoremap <down>  <nop>
nnoremap <left>  <nop>
nnoremap <right> <nop>
inoremap <up>    <nop>
inoremap <down>  <nop>
inoremap <left>  <nop>
inoremap <right> <nop>

" Map ctrl-movement keys to window switching
map <C-k> <C-w><Up>
map <C-j> <C-w><Down>
map <C-l> <C-w><Right>
map <C-h> <C-w><Left>
nnoremap <silent> <bs> <C-w><Left>

" Folding
" set foldenable
" set foldmethod=indent
" set foldlevel=20
" set sessionoptions+=folds
" augroup remember_folds
"     autocmd!
"     autocmd BufWinLeave *.* mkview
"     autocmd BufWinEnter *.* loadview
" augroup END
" augroup remember_folds
"     autocmd!
"     autocmd BufWrite,VimLeave *.* mkview
"     autocmd BufRead *.* silent loadview
" augroup END

" Save .swp in a single folder instead current dir
set dir=$HOME/.vim/tmp/swap
if !isdirectory(&dir) | call mkdir(&dir, 'p', 0700) | endif

" hilight  
au BufNewFile,BufRead *.ejs set filetype=html

" Uncomment the following to have Vim jump to the last position when
" " reopening a file
if has("autocmd")
      au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" ====================Plugins settings====================

" Appearance
let g:lightline = { 'colorscheme': 'solarized'  }
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#show_tab_type = 0 
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline_theme='gruvbox'
hi ColorColumn ctermbg=234 guibg=#1c1c1c
hi VertSplit   ctermbg=234 guibg=#1c1c1c
hi Folded      ctermfg=231 guibg=#121212 ctermbg=none
hi CursorLine  cterm=NONE ctermfg=NONE ctermbg=236

" Buffergator settings
" let g:buffergator_viewport_split_policy="B"


" Emmet configureation
let g:user_emmet_leader_key='<leader>e'

" This is for correct working autocomplete ^N in css files. Problem was in
" that CCS detected classes only without a dot. yup, I also add "-" for using
" BEM
function! CSS_BEM_support()
    set iskeyword-=. 
    set iskeyword-=-   
    set iskeyword-=_   
endfunction
augroup css
    autocmd!
    autocmd FileType css,scss,less,sass,html 
                \ autocmd VimEnter * call CSS_BEM_support()
    " support in session
    autocmd FileType css,scss,less,sass,html 
                \ autocmd SessionLoadPost * call CSS_BEM_support()
augroup END
 
" FZF
" Mapping selecting mappings
" nmap <leader><tab> <plug>(fzf-maps-n)
" xmap <leader><tab> <plug>(fzf-maps-x)
" omap <leader><tab> <plug>(fzf-maps-o)
" " Insert mode completion. 
" imap <c-x><c-k> <plug>(fzf-complete-word)
" imap <c-x><c-f> <plug>(fzf-complete-path)
" imap <c-x><c-j> <plug>(fzf-complete-file-ag)
" imap <c-x><c-l> <plug>(fzf-complete-line)"

"NERDTREE
let NERDTreeShowBookmarks=1
let NERDTreeAutoDeleteBuffer = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

"YCM
" Start autocompletion after 4 chars
" let g:ycm_min_num_of_chars_for_completion = 2
" let g:ycm_min_num_identifier_candidate_chars = 0
" let g:ycm_enable_diagnostic_highlighting = 1
" Don't show YCM's preview window [ I find it really annoying  ]
" set completeopt-=preview
" let g:ycm_add_preview_to_completeopt = 0

" ALE
" let g:ale_linters = {'js': ['eslint']}
" let g:ale_fixers = {'javascript': ['eslint'], 'vue': ['eslint']}
" let g:ale_javascript_eslint_use_global = 1
" let g:ale_lint_on_text_changed = 0
" let g:ale_lint_on_enter = 0
" let g:ale_lint_on_save = 1
" let g:ale_fix_on_save = 1
" let g:ale_linter_aliases = {'javascript.jsx': 'javascript', 'jsx': 'javascript','vue': 'javascript'}
" let g:ale_javascript_eslint_executable  = "/home/foobic/npm-global/bin/eslint"


"AMatchTagAlways
let g:mta_filetypes ={ 
    \'html':1,
    \'xml':1,
    \'javascript.jsx':1,
\ }

"VIM-Javascript
" set conceallevel=2
" let g:javascript_conceal_arrow_function = "⇒"


" ====================MAPPING====================
nnoremap <leader>bd :bd<cr> 
" nnoremap <C-i> :NERDTreeFind<CR>
nnoremap <silent> <leader><leader> :NERDTreeToggle<CR>
nnoremap <C-N> :bnext<CR>
nnoremap <C-P> :bprev<CR>
nnoremap w!! w !sudo tee > /dev/null %
nnoremap  <leader>w :w<cr> 
nnoremap  <leader>q :q<cr> 
nnoremap  <leader>ss :Obsession<cr> 
nnoremap  <leader>sd :Obsession!<cr> 
nnoremap  <leader>sr :source ./Session.vim<cr> 
cnoreabbrev wq1 wq!

nmap <silent> // :set hlsearch! hlsearch?<CR>

" buffegator
"format the entire file
nnoremap <leader>fef :normal! gg=G``<CR> 
nnoremap <leader>del :1,$ g/^$/ d <CR> 
" fzf
nnoremap <leader>f :FZF<CR> 
nnoremap <leader>bl :Buffers<CR> 

" close a buffer without losing split/window
nnoremap <leader>c :ene<CR>:bw #<CR>

" bindings for resizings pane
nnoremap <leader>j :resize +2<CR> 
nnoremap <leader>h :vertical resize +2<CR> 

" let GitGutterLineHighlightsEnable  = 1
" GitGutter
" nnoremap <leader>g :GitGutterToggle<CR> 

"YCM
" nnoremap \td :TernDef<CR> 
" nnoremap \tdp :TernDefPreview<CR> 
" nnoremap \tds :TernDefSplit<CR> 
" nnoremap \tR :TernRename<CR> 
" nnoremap \tt :TernType<CR> 
" nnoremap \tr :TernRefs<CR> 

"ALE
" nnoremap <leader>af :ALEFix<CR> 
" nnoremap <leader>at :ALEToggle<CR> 
" nnoremap <leader>ap :ALEPrevious<CR> 
" nnoremap <leader>an :ALENext<CR> 
"
" vim-fugitive
" nnoremap <leader>gs :Gstatus<CR> 
" nnoremap <leader>gc :Gcommit<CR> 
