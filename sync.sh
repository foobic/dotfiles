#!/bin/bash

cp ~/.bashrc ~/dotfiles/bashrc

# cp ~/.vimrc ~/dotfiles/vimrc
cp ~/.nvimrc ~/dotfiles/nvimrc
cp ~/.inputrc ~/dotfiles/inputrc

cp ~/.config/kitty/kitty.conf ~/dotfiles/kitty.conf
cp ~/.config/i3/config ~/dotfiles/i3/i3config
cp ~/.dircolors ~/dotfiles/i3/dircolors
cp /etc/i3blocks.conf ~/dotfiles/i3/i3blocks.conf
cp ~/for-i3/layswitch ~/dotfiles/i3/layswitch
# cp ~/.tmux.conf ~/dotfiles/tmux.conf
# cp ~/.tmux_theme1 ~/dotfiles/tmux_theme1
# cp ~/.tmux_theme2 ~/dotfiles/tmux_theme2

cp ~/.gitignore ~/dotfiles/gitignore
cp ~/.gitconfig ~/dotfiles/gitconfig

# touchpad gestures for Linux
cp ~/.config/libinput-gestures.conf ~/dotfiles/libinput-gestures.conf

# cp ~/.local/share/xfce4/terminal/colorschemes/jellybeans.theme ~/dotfiles/jellybeans.theme 

